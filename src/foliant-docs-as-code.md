# Docs as Code. Работаем с текстом как с кодом

## Предварительная установка необходимых компонентов

**Ubuntu:**

Все команды вводятся в терминале. Терминал открывается нажатием сочетания клавиш CTRL+ALT+T.

### Устанавливаем Git

```sh
$ sudo apt install git
```

### Настраиваем Git

```sh
$ git config --global user.name "John Ivanov"
$ git config --global user.email johnivanov@example.com
```
### Регистрируемся на GitLab

[https://gitlab.com/](https://gitlab.com/). В процессе регистрации нам понадобится придумать логин. Далее в примерах тут будет использоваться логин "Johnivan".

## Алгоритм работы со связкой Git + GitLab + Foliant

### Создаём проект на GitLab

* В интерфейсе GitLab нажимаем на "New project";
* В поле "Project name" вписываем название проекта, например, "test1";
* В настройке "Visibility level" для данного примера выбираем "Public";
* В пункте "Initialize repository with a README" галочку ставить не надо - README мы потом напишем сами;
* Нажимаем на "Create project" - в результате GitLab создаст нам пустой репозиторий для нашего проекта, и оповестит нас о том, что созданный репозиторий пуст: "The repository for this project is empty" (данный проект будет располагаться по URL [https://gitlab.com/Johnivan/test1](https://gitlab.com/Johnivan/test1)). Ниже мы увидим рекомендации о возможных дальнейших действиях - "Command line instructions". Игнорируем их;
* Склонируем этот пустой репозиторий на свой компьютер. Для этого в терминале запускаем команду:

```sh
$ git clone https://gitlab.com/Johnivan/test1
```

В результате в домашней директории на локальном компьютере создастся папка test1. Благодаря команде git clone, Git сам установит связь между этой папкой и удалённым репозиторием на GitLab. Нам уже ничего не надо для этого делать. Теперь у нас есть локальный пустой Git-репозиторий. Далее надо создать в нём Foliant-проект.

### Создаём Foliant-проект 

* В терминале переходим в папку test1:

```sh
$ cd test1
```

* В терминале, в папке test1, запускаем команду:

```sh
$ foliant init
```

Тут Foliant попросит нас ввести название проекта. Вводим наше название - "test1":

```sh
Enter the project name: test1
```

**Ожидаемый результат:**

```sh
$ foliant init
Enter the project name: test1
✔ Generating Foliant project
─────────────────────
Project "test1" created in test1
```

В результате Foliant создал на нашем компьютере папку test1, содержащую компоненты Foliant-проекта, вот с такой структурой:

```
├── docker-compose.yml
├── Dockerfile
├── foliant.yml
├── README.md
├── requirements.txt
└── src
    └── index.md
```

Но (!) получилось так, что папка test1, которую создал Foliant, расположилась в папке test1, которую создал Git. Нам это не подходит.

* С компьютера вырезаем все объекты из папки test1, которую создал Foliant, и вставляем на уровень выше - в папку test1, которую до этого создал Git. А пустую теперь папку test1, которую создал Foliant, удаляем;

* Среди оставшихся объектов находим файл README.md. Удаляем из него всё содержимое, кроме заголовка. Остаётся только заголовок:

```sh
#   test1
```

Это будет наш файл README.md;

* Открываем и редактируем конфигурационный файл Foliant - foliant.yml: 

#### Между строками title и chapters добавляем конфигурацию бэкенда Mkdocs:

```sh
backend_config:
  mkdocs:
    mkdocs_path: mkdocs
    slug: test1
    use_title: true
    use_chapters: true
    use_headings: true
    default_subsection_title: Expand
    mkdocs.yml:
      site_name: test1
      site_url: https://johnivan.gitlab.io/test1
```

Тут следует обратить внимание на следующие параметры:

**mkdocs_path** - это папка, которую создаст Mkdocs, чтобы сохранить в неё сгенерированный сайт;

**slug** - это слаг, из которого mkdocs создаст название сайта. Может быть любым. В данном случае мы вписали слаг, совпадающий с названием нашего проекта. Это значит, что сгенерированный сайт будет иметь имя test1.mkdocs;

**site_name** - это название сайта, которое будет отображаться в шапке сайта;

**site_url** - это будущий адрес сайта в GitLab. Сайта ещё нет, но мы заранее знаем, каким будет его адрес. Структура такая: в поддомене наш логин в GitLab (Johnivan) -> домен gitlab.io -> через слэш - название нашего проекта (test1). В данном примере получается: https://johnivan.gitlab.io/test1.

#### Добавляем список страниц нашего сайта:

Обычно добавляем. Но сейчас не добавляем. Т.к. по умолчанию туда уже добавлена индексная страница - index.md - сейчас ничего менять не будем, оставим как есть:

```sh
chapters:
  - index.md
```

В итоге должен получиться файл с таким содержимым:

```sh
 title: test1

 backend_config:
   mkdocs:
     mkdocs_path: mkdocs
     slug: test1
     use_title: true
     use_chapters: true
     use_headings: true
     default_subsection_title: Expand
     mkdocs.yml:
       site_name: test1
       site_url: https://johnivan.gitlab.io/test1

 chapters:
   - index.md
```

Обратите внимание, что в получившемся файле строки расположены иерархически на трёх уровнях. Важно, чтобы строки одного уровня располагались строго друг под другом, а строки каждого нижестоящего уровня отступали от вышестоящего на два пробела. Например, в получившемся файле строка backend_config располагается строго под строкой title, а строка site_name отступает от строки mkdocs.yml ровно на два пробела.

Сохраняем, закрываем.

### Добавляем наш Foliant-проект в Git

Чтобы Git отслеживал изменения, которые мы в дальнейшем будем вносить в ходе работы над проектом, в терминале, в папке проекта test1, вводим команду:

```sh
$ git add --all
```

### Делаем коммит в Git

Зафиксируем снимок состояния нашего проекта на текущий момент. Кроме того, добавим описание коммита - какая задача была решена. В данном случае нашей задачей являлось зафиксировать первоначальное состояние проекта, поэтому в описании коммита напишем просто "initial commit". В терминале запускаем команду:

```sh
$ git commit -a -m 'initial commit'
```

### Выгружаем наш Foliant-проект на GitLab

В терминале запускаем команду:

```sh
$ git push
```

На данном этапе потребуется ввести логин и пароль от аккаунта на GitLab.

### Настраиваем сборку проекта в GitLab CI 

* В браузере переходим на страницу нашего проекта на GitLab: [https://gitlab.com/Johnivan/test1](https://gitlab.com/Johnivan/test1) (в адресе замените "Johnivan" на ваш логин на GitLab). Входим в свой аккаунт;

* Мы хотим, чтобы в GitLab CI происходила сборка нашего проекта - то есть, чтобы GitLab CI проходился по конфигурационным файлам нашего проекта и исполнял нужные нам скрипты. Значит, нам понадобится добавить конфигурационный файл для GitLab CI. Для этого на странице нашего проекта на GitLab ([https://gitlab.com/Johnivan/test1](https://gitlab.com/Johnivan/test1)) нажимаем на кнопку "Set up CI/CD". Открывшееся поле заполняем таким содержимым:

```sh
image: 
  name: foliant/foliant:1.0.8
  entrypoint: [""]

before_script:
  - pip3 install -r ./requirements.txt

pages:
  stage: deploy
  script:
    - foliant make site -w mkdocs
    - mv test1.mkdocs public
  only:
  - master
  artifacts:
    paths:
    - public
```

Тут стоит обратить внимание на строку:

```
- mv test1.mkdocs public
```
В этой строке **test1.mkdocs** - это имя нашего сайта с расширением mkdocs.

Под полем "Set up CI/CD" мы видим ещё два поля:

**Commit message** - по умолчанию уже заполнено сообщением "Add .gitlab-ci.yml". Так и оставим.

**Target Branch** - целевая ветка. По умолчанию уже проставлено Master. Мы в данном случае всё добавляем в ветку Master, поэтому тоже оставляем как есть.


* Нажимаем на кнопку "Commit changes" - файл .gitlab-ci.yml добавлен в структуру нашего проекта;

* На странице нашего проекта на GitLab ([https://gitlab.com/Johnivan/test1/](https://gitlab.com/Johnivan/test1/)) в боковом меню выбираем пункт "CI/CD" (пиктограмма "ракета"), там переходим в подменю "Jobs". Нам нужно, чтобы в колонке "Status" высветился результат "passed" - значит, работа выполнена успешно;

* На странице нашего проекта на GitLab ([https://gitlab.com/Johnivan/test1/](https://gitlab.com/Johnivan/test1/)) в боковом меню выбираем пункт "Settings" (пиктограмма "шестерёнка"), там переходим в подменю "Pages". Находим строки:


>Access pages

>**Congratulations! Your pages are served under:**

>[https://johnivan.gitlab.io/test1](https://johnivan.gitlab.io/test1)

Это значит, что GitLab Pages создал для нас URL, по которому теперь расположен наш сгенерированный сайт. Переходим, любуемся. Готово.

