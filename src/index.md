# С чего начать? Навигация по руководству

* Понять Foliant >> [Видео про Foliant (40 минут)](https://www.youtube.com/watch?v=mJ5IqzmXuMo);

* Быстро испытать Foliant в деле >> ["Hello, World!" на Foliant](foliant-hello-world.md);

* Поработать с текстом как с кодом >> [Docs as Code с использованием Foliant](foliant-docs-as-code.md);

* Найти ответ на свой вопрос >> [How-To - короткие инструкции для конкретных задач](foliant-how-to.md).
